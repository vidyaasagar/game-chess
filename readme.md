# HCR Test01
1. Using the packages mentioned below, these pages have to be created.
	a. Start chess game
	b. Join chess game
	c. Game should stop & winner has to be decided based on time, user can choosen time (1 min, 2 mins, 5 mins, 10 mins, 15 mins & so on) before starting or joining game.

2. Front-end packages(Please get the latest packges):
	
	```json
	"dependencies": {
	    "bootstrap": "", //Optional
	    "jquery": "", //Optional
	    "highcharts": "", //Optional
	    "moment": "", 
	    "lodash": "", 
	    "messenger": "", //Optional
	    "fontawesome": "", //Optional
	    "chessjs": "^juangl/chessjs#0.9.0",
	    "chessboardjs": "^vstene/chessboardjs#0.3.2" //Has better board than regular chessboard.js
	}
	```
3. Back-end packages(Please get the latest packges):
	
	```json
	"dependencies": {
	    "body-parser": "",
	    "chess.js": "",
	    "config": "",
	    "connect-flash": "",
	    "cookie-parser": "",
	    "debug": "",
	    "elasticsearch": "", //Good to use
	    "express": "",
	    "express-session": "^",
	    "jade": "",
	    "lodash": "",
	    "moment": "",
	    "mongoose": "",
	    "morgan": "",
	    "passport": "",
	    "passport-local": "",
	    "serve-favicon": "",
		"socket.io": ""
	}
	``` 
4. All aspects of code will taken to considration in terms of evaluation.
5. Task has to be completed in 48 hours
6. Create new branch and push your code there for review.


