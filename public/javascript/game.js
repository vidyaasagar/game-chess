var init = function() {

//--- start example JS ---
var board,
  game = new Chess();
var clickPause =false;
//var z;
// Initites the game timer from timer.js. current game time can be checked via moment.duration method
var y=setTime();


function myPauseFunction(){
       
    clearInterval(z);
    clickPause = true;
    console.log("Pause function invoked !");
}
function myResumeFunction(){
       
    
    clickPause = false;
    z = setInterval(timeCounter, 1000);
    // setInterval()
    console.log("Resume function invoked !");
}



// do not pick up pieces if the game is over
// only pick up pieces for White
var onDragStart = function(source, piece, position, orientation) {
  if (game.inCheckmate() === true || game.inDraw() === true ||
    piece.search(/^b/) !== -1) {
    return false;
  }
};


var makeRandomMove = function() {
  var possibleMoves = game.moves();

  // game over
  if (possibleMoves.length === 0) {return alert("Game Over !")};

  var randomIndex = Math.floor(Math.random() * possibleMoves.length);
  game.move(possibleMoves[randomIndex]);
  board.position(game.fen());
};
 

var onDrop = function(source, target) {
  // see if the move is legal
  if(!clickPause){
  duration = moment.duration(duration - interval, 'milliseconds');
  }
  console.log(duration._milliseconds);
  if(duration._milliseconds < -1 || clickPause){
    console.log("game over");
    return 'snapback';
   } 
   else{
     var move = game.move({
    from: source,
    to: target,
    promotion: 'q' // NOTE: always promote to a queen for example simplicity
    });
   }
  

  // illegal move
  if (move === null) return 'snapback';

  // make random legal move for black
  window.setTimeout(makeRandomMove, 250);
};

// update the board position after the piece snap
// for castling, en passant, pawn promotion
var onSnapEnd = function() {
  board.position(game.fen());
};

var onMoveEnd = function(oldPos, newPos) {
  console.log("Move animation complete:");
  console.log("Old position: " + ChessBoard.objToFen(oldPos));
  console.log("New position: " + ChessBoard.objToFen(newPos));
  console.log("--------------------");
};



var cfg = {
  draggable: true,
  position: 'start',
  onDragStart: onDragStart,
  onDrop: onDrop,
  onSnapEnd: onSnapEnd,
  onMoveEnd: onMoveEnd
};

board = ChessBoard('board', cfg);
//--- end example JS ---

$('#pause').on('click',myPauseFunction);
$('#resume').on('click',myResumeFunction);


}; // end init()
$(document).ready(init);